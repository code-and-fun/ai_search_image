### AI Search Image
- Đầu vào là 1 hình ảnh, nó sẽ hiển thị các ảnh giống thế trong kho dữ liệu

## Cài đặt 
- install `python3`, `opencv`, `flask` và các Dependencies cần thiết
- clone project
- update dataset: `python3 index.py --dataset dataset --index index.csv`
- run: `python3 search.py`
- example: http://127.0.0.1:5000/search?query=queries/127502.png

