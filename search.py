# USAGE
# python search.py --index index.csv --query queries/103100.png --result-path dataset

# import the necessary packages
from pyimagesearch.colordescriptor import ColorDescriptor
from pyimagesearch.searcher import Searcher
from flask import Flask, jsonify, request
import cv2

app = Flask(__name__)

@app.route("/search")
def main():
    # initialize the image descriptor
    response = []
    cd = ColorDescriptor((8, 12, 3))

    #env
    index = "index.csv"
    query = request.args.get('query')
    result_path = "dataset"

    # load the query image and describe it
    query = cv2.imread(query)
    features = cd.describe(query)

    # perform the search
    searcher = Searcher(index)
    results = searcher.search(features, 20)

    # loop over the results
    for (score, resultID) in results:
        # load the result image and display it
        response.append(result_path + "/" + resultID)
    return jsonify(response)

if __name__ == "__main__":
  app.run()

